#!/bin/bash

echo -e "Welcome Zaryob web page downloader"

echo -e "Please input web page:"
read page

echo -e "Please input target dir:"
read target

workdir=$(pwd)
{ # try
    if [ -z "$target"  ]
    then
        echo -e "\033[33m Using work directory!! \033[0m"

    else
        echo -e "\033[33m Downloading web page into -> $target \033[0m"

        mkdir $target &&\
        cd $target 
    fi

    wget --limit-rate=200k --no-clobber \
         --convert-links --random-wait -r \
         -p -E -e robots=off -U mozilla $page && \
    cd $workdir
    echo -e """ \033[32m Downloaded web page :) \033[0m """

} || { # catch
    echo -e """\033[31m Errored :( \033[0m """
}

