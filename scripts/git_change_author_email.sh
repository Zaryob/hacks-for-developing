#!/bin/sh

echo """You changed your email address but did not update in the github repository.\nDo not worry, this script will being handful.\n\n"""
OLD_EMAIL="old_email_address@old_email.com"
CORRECT_NAME="your name"
CORRECT_EMAIL="new_email_address@new_email.com"

git filter-branch --env-filter '

if [ "$GIT_COMMITTER_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_COMMITTER_NAME="$CORRECT_NAME"
    export GIT_COMMITTER_EMAIL="$CORRECT_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_AUTHOR_NAME="$CORRECT_NAME"
    export GIT_AUTHOR_EMAIL="$CORRECT_EMAIL"
fi
' --tag-name-filter cat -- --branches --tags
